<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter;

interface ObjectToIndexConverterInterface
{
    public function convertToIndexableValue($object);
}