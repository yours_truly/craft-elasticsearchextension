<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSearch;

class QueryParser
{
    private $queryParts;
    private $filters;
    public function parseQuery($query) {
        $this->queryParts = [];
        $this->filters = [];
        $parts = explode(' ', $query);
        foreach ($parts as $part) {
            if ($filter = $this->parseFilter($part)) {
                $this->filters[] = $filter;
            }
            else {
                $this->queryParts[] = $part;
            }
        }
    }

    public function getQueryPart() {
        return implode(' ', $this->queryParts);
    }

    public function getFilters() {
        return $this->filters;
    }

    private function parseFilter($part) {
        $filterParts = explode(':', $part);
        if(count($filterParts) === 2 && strlen($filterParts[0]) && strlen($filterParts[1])) {
            return [
                'field' => $filterParts[0],
                'value' => $filterParts[1],
                'operator' => 'term'
            ];
        }
        return null;
    }
}