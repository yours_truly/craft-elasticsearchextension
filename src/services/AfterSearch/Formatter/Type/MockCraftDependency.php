<?php

namespace yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\Type;

class MockCraftDependency implements \ArrayAccess, \Countable, \Iterator
{
    private $container;
    private $position = 0;
    public function __construct($data)
    {
        $this->container = $data;
    }

    public function current()
    {
        return $this->container[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return isset($this->container[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }


    public function one() {
        return $this->container[0] ?? null;
    }
    public function all() {
        return $this->container;
    }


    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    public function count()
    {
      return count($this->container);
    }



}