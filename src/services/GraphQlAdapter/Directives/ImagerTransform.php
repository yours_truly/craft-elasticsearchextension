<?php

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Directives;

use craft\elements\Asset;
use craft\gql\GqlEntityRegistry;
use GraphQL\Language\DirectiveLocation;
use GraphQL\Type\Definition\Directive as GqlDirective;
use GraphQL\Type\Definition\ResolveInfo;
use spacecatninja\imagerx\exceptions\ImagerException;
use spacecatninja\imagerx\gql\arguments\ImagerTransformArguments;
use spacecatninja\imagerx\ImagerX;
use spacecatninja\imagerx\services\ImagerService;

class ImagerTransform extends \spacecatninja\imagerx\gql\directives\ImagerTransform
{
    /**
     * @inheritdoc
     */
    public static function create(): GqlDirective
    {
        if ($type = GqlEntityRegistry::getEntity(self::class)) {
            return $type;
        }

        $type = GqlEntityRegistry::createEntity(static::name(), new self([
            'name' => static::name(),
            'locations' => [
                DirectiveLocation::FIELD,
            ],
            'args' => ImagerTransformArguments::getArguments(),
            'description' => 'This directive is used to return a URL for an using Imager X.'
        ]));

        return $type;
    }
    /**
     * @inheritdoc
     */
    public static function apply(mixed $source, mixed $value, array $arguments, ResolveInfo $resolveInfo): mixed
    {
        if ($source instanceof Asset) {
            return parent::apply($source, $value, $arguments, $resolveInfo);
        }
        if ($resolveInfo->fieldName !== 'url') {
            return $value;
        }

        if (!is_array($source) || !isset($source['url'])) {
            return null;
        }
        $returnType = 'url';

        if (isset($arguments['return'])) {
            $returnType = $arguments['return'];
            unset($arguments['return']);
        }

        $transform = $arguments;

        if ( !\in_array(strtolower(self::getExtension($source['url'])), ImagerService::getConfig()->safeFileFormats, true)) {
            return null;
        }

        try {
            $transformedImage = ImagerX::$plugin->imagerx->transformImage(self::getRelativeUrl($source['url']), $transform);
        } catch (ImagerException $e) {
            \Craft::error('An error occured when trying to transform image in GraphQL directive: ' . $e->getMessage(), __METHOD__);
            return null;
        }

        if ($returnType === 'base64') {
            return $transformedImage->getBase64Encoded();
        }

        if ($returnType === 'dataUri') {
            return $transformedImage->getDataUri();
        }

        if ($returnType === 'blurhash') {
            return $transformedImage->getBlurhash();
        }

        return $transformedImage->getUrl();
    }

    private static function getExtension($url) {
        $pathParts = pathinfo($url);
        return  $pathParts['extension'];
    }
    private static function getRelativeUrl($url) {
        $pathParts = parse_url($url);
        return  $pathParts['path'];
    }
}