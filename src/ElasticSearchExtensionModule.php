<?php
/**
 * ElasticSearchExtension module for Craft CMS 3.x
 *
 * Extend the craft ES Plugin
 *
 * @link      soerenparton.de
 * @copyright Copyright (c) 2021 Sören Parton
 */

namespace yourstruly\elasticsearchextensionmodule;

use craft\elements\Entry;
use craft\errors\InvalidFieldException;
use craft\events\ExecuteGqlQueryEvent;
use craft\events\RegisterGqlDirectivesEvent;
use craft\events\RegisterGqlQueriesEvent;
use craft\events\RegisterGqlTypesEvent;
use craft\services\Gql;
use craft\services\Plugins;
use craft\web\twig\variables\CraftVariable;
use lhs\elasticsearch\events\SearchEvent;
use lhs\elasticsearch\records\ElasticsearchRecord;
use lhs\elasticsearch\variables\ElasticsearchVariable;
use yourstruly\elasticsearchextensionmodule\assetbundles\elasticsearchextensionmodule\ElasticSearchExtensionModuleAsset;
use yourstruly\elasticsearchextensionmodule\services\ElasticSearchExtensionModuleService as ElasticSearchExtensionModuleServiceService;

use Craft;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\TemplateEvent;
use craft\i18n\PhpMessageSource;
use craft\web\View;

use yourstruly\elasticsearchextensionmodule\variables\ElasticSearchExtensionVariable;
use spacecatninja\imagerx\gql\directives\ImagerSrcset;
use spacecatninja\imagerx\gql\directives\ImagerTransform;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Directives\ImagerTransform as MyImagerTransform;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Sören Parton
 * @package   ElasticSearchExtensionModule
 * @since     1.0.0
 *
 * @property  ElasticSearchExtensionModuleServiceService $elasticSearchExtensionModuleService
 */
class ElasticSearchExtensionModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * ElasticSearchExtensionModule::$instance
     *
     * @var ElasticSearchExtensionModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@yourstruly/elasticsearchextensionmodule', $this->getBasePath());
        $this->controllerNamespace = 'yourstruly\elasticsearchextensionmodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@yourstruly/elasticsearchextensionmodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Base template directory
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $e) {
            if (is_dir($baseDir = $this->getBasePath().DIRECTORY_SEPARATOR.'templates')) {
                $e->roots[$this->id] = $baseDir;
            }
        });
        Event::on(View::class, View::EVENT_REGISTER_SITE_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $event) {
            $event->roots['elasticsearchextensionmodule'] = __DIR__ . '/templates';
        });

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * ElasticSearchExtensionModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;


        Event::on(ElasticsearchRecord::class, ElasticsearchRecord::EVENT_BEFORE_SEARCH, function (SearchEvent $event) {
            ElasticSearchExtensionModule::$instance->elasticSearchExtensionModuleService->onBeforeSearch($event);
        });

        Event::on(ElasticsearchRecord::class, ElasticsearchRecord::EVENT_INIT, function (Event $event) {
            ElasticSearchExtensionModule::$instance->elasticSearchExtensionModuleService->onInit($event);
        });

        Event::on(ElasticsearchRecord::class, ElasticsearchRecord::EVENT_BEFORE_CREATE_INDEX, function (Event $event) {
            ElasticSearchExtensionModule::$instance->elasticSearchExtensionModuleService->onBeforeCreateIndex($event);
        });

        Event::on(ElasticsearchRecord::class, ElasticsearchRecord::EVENT_BEFORE_SAVE, function (Event $event) {
            ElasticSearchExtensionModule::$instance->elasticSearchExtensionModuleService->onBeforeSave($event);
        });
        Craft::$app->getGql()->on(Gql::EVENT_BEFORE_EXECUTE_GQL_QUERY,function (ExecuteGqlQueryEvent $event) {
            ElasticSearchExtensionModule::$instance->esToGqlAdapterService->onBeforeExecuteGqlQuery($event);
        });

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_LOAD_PLUGINS,
            function() {
                Event::on(Gql::class,
                    Gql::EVENT_REGISTER_GQL_DIRECTIVES,
                    static function(RegisterGqlDirectivesEvent $event) {
                        $event->directives = array_diff($event->directives, [ImagerTransform::class]);
                        $event->directives[] = MyImagerTransform::class;
                    }
                );
            }
        );

        // Register variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('elasticsearchExtension', ElasticSearchExtensionVariable::class);
            }
        );



        Craft::info(
            Craft::t(
                'elastic-search-extension-module',
                '{name} module loaded',
                ['name' => 'ElasticSearchExtension']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
