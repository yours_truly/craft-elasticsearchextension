<?php

namespace yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter;

interface FormatterInterface
{
    public function format($result);
}