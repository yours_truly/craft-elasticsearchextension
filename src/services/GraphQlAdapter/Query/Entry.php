<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Query;

use craft\gql\arguments\elements\Entry as EntryArguments;
use craft\gql\base\Query;
use craft\gql\types\QueryArgument;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces\Entry as EntryInterface;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Resolvers\Entry as EntryResolver;
use craft\helpers\Gql as GqlHelper;
use GraphQL\Type\Definition\Type;

/**
 * Class Entry
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.3.0
 */
class Entry extends Query
{
    private static $arguments = [];
    public static function registerArgument($key, $config) {
        self::$arguments[$key] = $config;
    }
    public static function getArguments() {
        $defaultArguments = [
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limits the query results to n results',
            ],
            'offset' => [
                'name' => 'offset',
                'type' => Type::int(),
                'description' => 'Offset',
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'Order By',
            ],
            'search' => [
                'name' => 'search',
                'type' => Type::string(),
                'description' => 'SearchString',
            ],
        ];
        return array_merge($defaultArguments, self::$arguments);
    }
    /**
     * @inheritdoc
     */
    public static function getQueries($checkToken = true): array
    {

        return [
            'entries' => [
                'type' => Type::listOf(\yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces\Entry::getType()),
                'args' => self::getArguments(),
                'resolve' => EntryResolver::class . '::resolve',
                'description' => 'This query is used to query for entries.',
                'complexity' => GqlHelper::relatedArgumentComplexity(),
            ],
            'entryCount' => [
                'type' => Type::nonNull(Type::int()),
                'args' => self::getArguments(),
                'resolve' => EntryResolver::class . '::resolveCount',
                'description' => 'This query is used to return the number of entries.',
                'complexity' => GqlHelper::singleQueryComplexity(),
            ],
            'entry' => [
                'type' => \yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces\Entry::getType(),
                'args' => self::getArguments(),
                'resolve' => EntryResolver::class . '::resolveOne',
                'description' => 'This query is used to query for a single entry.',
                'complexity' => GqlHelper::singleQueryComplexity(),
            ],
        ];
    }
}
