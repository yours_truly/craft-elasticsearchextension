<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types;

use craft\elements\Entry as EntryElement;
use craft\gql\base\ObjectType;
use craft\gql\GqlEntityRegistry;
use craft\gql\interfaces\elements\Entry as EntryInterface;
use craft\gql\types\DateTime;
use craft\helpers\Gql as GqlHelper;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ObjectType as GqlObjectType;
use GraphQL\Type\Definition\Type;

/**
 * Class Entry
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.3.0
 */
class Image extends GqlObjectType
{
    private static $additionalFieldConfig = [];
    /** @var callable[]  */
    private static $additionalResolverConfig = [];

    public function __construct(array $config = [])
    {

        parent::__construct([
            'name' => self::getName(),
            'fields' => function()  {
                return [
                    'id' => ['type' => Type::int() ],
                    'url' => ['type' => Type::string(),

                        ],
                    'copyright' => ['type' => Type::string() ],
                    'caption' => ['type' => Type::string() ],
                    'extension' => ['type' => Type::string() ],
                ];
            },
            'resolveField' => function ($rootValue, $args, $context, ResolveInfo $info) {
                return $this->resolve($rootValue, $args, $context, $info);
            }
        ]);
    }
    public static function getType(): self
    {
        return GqlEntityRegistry::getEntity(self::getName()) ?: GqlEntityRegistry::createEntity(self::getName(), new self());
    }

    public static function getName() {
        return 'ElasticSearchImage';
    }


    /**
     * @inheritdoc
     */
    protected function resolve($source, $arguments, $context, ResolveInfo $resolveInfo)
    {
        $fieldName = GqlHelper::getFieldNameWithAlias($resolveInfo, $source, $context);
        $value = $source[$fieldName];
        return  GqlHelper::applyDirectives($source, $resolveInfo, $value);
    }
}
