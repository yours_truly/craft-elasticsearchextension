<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator;

use craft\base\Element;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy\ReduceStrategyInterface;

class RelationValueDeterminator implements ValueDeterminatorInterface
{
    /** @var string */
    private $fieldHandle;
    private $queryValues;
    private $reduceStrategy;
    public function __construct(string $fieldHandle, string $queryValues, ReduceStrategyInterface $reduceStrategy)
    {
        $this->fieldHandle = $fieldHandle;
        $this->queryValues = $queryValues;
        $this->reduceStrategy = $reduceStrategy;
    }

    public function determineValue(Element $element)
    {
        switch ($this->queryValues) {
            case 'one':
                $relations = [$element->getFieldValue($this->fieldHandle)->one()];
                break;
            case 'all':
                $relations = $element->getFieldValue($this->fieldHandle)->all();
        }
        return $this->reduceStrategy->reduceValue($relations);
    }

}