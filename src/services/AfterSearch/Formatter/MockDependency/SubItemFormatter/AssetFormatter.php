<?php

namespace yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\MockDependency\SubItemFormatter;

use craft\elements\Asset;
use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\FormatterInterface;

class AssetFormatter implements FormatterInterface
{
    public function format($result)
    {
        $asset = new Asset();
        $asset->getSerializedFieldValues();
        $asset->volumeId = $result['url'];
        $asset->id = $result['id'];
        return $asset;
    }

}