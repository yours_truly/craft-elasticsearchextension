<?php

namespace yourstruly\elasticsearchextensionmodule\variables;

use craft\elements\Asset;
use lhs\elasticsearch\Elasticsearch;
use lhs\elasticsearch\queries\ElasticsearchQuery;
use yourstruly\elasticsearchextensionmodule\ElasticSearchExtensionModule;

class ElasticSearchExtensionVariable
{
    public function isEsEnabled() {
        return ElasticSearchExtensionModule::getInstance()->elasticSearchExtensionModuleService->isElasticSearchEnabled();
    }
    public function isAsset($var) {
        return $var instanceof Asset;
    }
    public function buildQuery() {
        return ElasticSearchExtensionModule::getInstance()->elasticSearchExtensionModuleService->buildQuery();
    }
}