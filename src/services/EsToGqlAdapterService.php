<?php

namespace yourstruly\elasticsearchextensionmodule\services;
use Craft;
use craft\base\Component;
use craft\events\ExecuteGqlQueryEvent;
use craft\events\RegisterGqlQueriesEvent;
use craft\events\RegisterGqlTypesEvent;
use craft\gql\base\GeneratorInterface;
use craft\gql\GqlEntityRegistry;
use craft\gql\TypeLoader;
use craft\gql\types\Query;
use craft\helpers\StringHelper;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\Error;
use GraphQL\GraphQL;
use GraphQL\Type\Definition\ObjectType as GqlObjectType;
use GraphQL\Type\Schema;
use yourstruly\elasticsearchextensionmodule\ElasticSearchExtensionModule;
use yourstruly\elasticsearchextensionmodule\services\Factory\CraftToElasticSearchAdapterFactory;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces\Entry as EntryInterface;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Query\Entry as EntryQuery;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types\Entry as EntryType;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types\Image;

class EsToGqlAdapterService extends Component
{
    private $config;

    public function __construct($config = [])
    {
        $this->config = $config;
        unset($config['additionalFields']);
        unset($config['esEnabled']);
        parent::__construct($config);
    }
    /**
     * Event Handler registered in module
     * Either the query can be handled by ES or we get an exception, flush the GQL registries and let Craft's GQL give a try.
     * @param ExecuteGqlQueryEvent $event
     */
    public function onBeforeExecuteGqlQuery(ExecuteGqlQueryEvent $event) {
        if (ElasticSearchExtensionModule::$instance->elasticSearchExtensionModuleService->isElasticSearchEnabled()) {
            $debugMode = true;
            $isIntrospectionQuery = StringHelper::containsAny($event->query, ['__schema', '__type']);

            $gqlCraftSimulatorService = new GqlCraftSimulatorService();
            //Simulate the craft schema first...
            //(we can't use the actual craft service because we would interfere with the services schema caches)
            $schemaDef = $gqlCraftSimulatorService->buildCraftSchema();
            $config = $schemaDef->getConfig();
            //Before adjusting it
            $this->addGqlTypes($config->types);
            $queries = $config->query->getFields();
            $this->adjustQueries($queries);
            $config->query = GqlEntityRegistry::createEntity(static::class, new GqlObjectType([
                'name' => Query::getName(),
                'fields' => $queries,
            ]));
            $config->mutation = null;
            try {
                $newSchemaDef = new Schema($config);
                $event->result = GraphQL::executeQuery(
                    $newSchemaDef,
                    $event->query,
                    $event->rootValue,
                    $event->context,
                    $event->variables,
                    $event->operationName,
                    null,
                    Craft::$app->getGql()->getValidationRules($debugMode, $isIntrospectionQuery)
                )
                    ->setErrorsHandler([$this, 'handleQueryErrors'])
                    ->toArray($debugMode ? DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE : false);
                $event->result['isElasticSearch'] = 1;
            }
            catch (\Throwable $e) {
                //Catch error to be able to fall back to Craft's DB based GQL query
                //Flush registries before craft tries to fulfill the query
                $gqlCraftSimulatorService->flushRegistries();
            }
        }
    }

    /**
     * Custom error handler for GraphQL query errors
     *
     * @param Error[] $errors
     * @param callable $formatter
     * @return Error[]
     * @since 3.6.2
     */
    public function handleQueryErrors(array $errors, callable $formatter)
    {
        $reasonString = '';
        foreach ($errors as $error) {
            $reasonString .= $error->getMessage() . ' ';
        }
        Craft::warning('GQL-ElasticSearch: Falling back to DB-Query. Reason: ' . $reasonString, self::class);
        throw new \Exception('Dummy');
    }

    /**
     * @return string[]
     */
    private function getAdditionalGqlTypes() {
        return [
            EntryInterface::class,
            Image::class
        ];
    }

    /**
     * Add or own configured query arguments to the entry query
     * @param $queries
     * @param bool $register
     */
    private function adjustQueries(&$queries, $register = true) {
        foreach ($this->config['additionalFields'] as $fieldHandle => $fieldConfig) {
            if (isset($fieldConfig['graphQL']['registerAsArgument'])) {
                $config = isset($fieldConfig['graphQL']['registerAsArgument']['configCallback']) ?
                    $fieldConfig['graphQL']['registerAsArgument']['configCallback']() :
                    $fieldConfig['graphQL']['registerAsArgument']['config'];
                EntryQuery::registerArgument($fieldConfig['graphQL']['registerAsArgument']['handle'],
                    $config);
            }
        }

        $overrideQueries = EntryQuery::getQueries();
        foreach ($overrideQueries as $key => $query) {
            $queries[$key] = $query;
        }
        if ($register) {
            TypeLoader::registerType('Query', function() use ($queries) {
                return call_user_func(Query::class . '::getType', $queries);
            });
        }
    }

    /**
     * Adding our own versions of GQL Types
     * @param $types
     * @param bool $register
     */
    private function addGqlTypes(&$types, $register = true) {
        foreach ($this->config['additionalFields'] as $fieldHandle => $fieldConfig) {
            if (isset($fieldConfig['graphQL']['registerAsTypeField'])) {
                $resolver = $fieldConfig['graphQL']['registerAsTypeField']['resolver'] ?? function($source) use ($fieldHandle) {
                        return $source->{$fieldHandle};
                    };
                $config = isset($fieldConfig['graphQL']['registerAsTypeField']['configCallback']) ?
                    $fieldConfig['graphQL']['registerAsTypeField']['configCallback']() :
                    $fieldConfig['graphQL']['registerAsTypeField']['config'];
                EntryType::addFieldConfig($fieldConfig['graphQL']['registerAsTypeField']['handle'],
                    $config, $resolver);
            }
        }
        foreach ($this->getAdditionalGqlTypes() as $type) {
            if ($register) {
                TypeLoader::registerType($type::getName(), $type . '::getType');
                if (method_exists($type, 'removeConflictingTypes')) {
                    $type::removeConflictingTypes($types);
                }
                if (method_exists($type, 'getTypeGenerator')) {
                    /** @var GeneratorInterface $typeGeneratorClass */
                    $typeGeneratorClass = $type::getTypeGenerator();

                    if (is_subclass_of($typeGeneratorClass, GeneratorInterface::class)) {
                        foreach ($typeGeneratorClass::generateTypes() as $generateType) {
                            $types[] = $generateType;
                        }
                    }
                }
            }
        }

    }


}