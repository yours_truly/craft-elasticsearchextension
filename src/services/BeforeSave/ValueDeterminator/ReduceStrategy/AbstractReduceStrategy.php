<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy;

use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter\ObjectToIndexConverterInterface;

abstract class AbstractReduceStrategy implements ReduceStrategyInterface
{
    /** @var ObjectToIndexConverterInterface */
    protected $objectConverter;
    public function __construct($objectConverter)
    {
        $this->objectConverter = $objectConverter;
    }
}