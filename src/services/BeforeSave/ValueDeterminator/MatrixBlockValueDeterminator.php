<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator;

use craft\base\Element;
use craft\elements\MatrixBlock;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy\ReduceStrategyInterface;

class MatrixBlockValueDeterminator implements ValueDeterminatorInterface
{
    /** @var string */
    private $fieldHandle;
    /** @var string */
    private $blockFieldHandle;
    /** @var string */
    private $blockTypeHandle;
    /** @var ReduceStrategyInterface  */
    private $reduceStrategy;
    public function __construct(string $fieldHandle, string $blockFieldHandle, string $blockTypeHandle, ReduceStrategyInterface $reduceStrategy)
    {
        $this->fieldHandle = $fieldHandle;
        $this->blockFieldHandle = $blockFieldHandle;
        $this->blockTypeHandle = $blockTypeHandle;
        $this->reduceStrategy = $reduceStrategy;
    }

    public function determineValue(Element $element)
    {
        $relations = [];
        $blockField = clone $element->getFieldValue($this->fieldHandle);
        $blocks = $blockField->type($this->blockTypeHandle)->all();
        /** @var MatrixBlock $block */
        foreach ($blocks as $block) {
            $relations = array_merge((clone $block->getFieldValue($this->blockFieldHandle))->all(), $relations);
        }

        return $this->reduceStrategy->reduceValue($relations);
    }

}