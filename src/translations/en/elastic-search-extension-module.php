<?php
/**
 * ElasticSearchExtension module for Craft CMS 3.x
 *
 * Extend the craft ES Plugin
 *
 * @link      soerenparton.de
 * @copyright Copyright (c) 2021 Sören Parton
 */

/**
 * ElasticSearchExtension en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('elastic-search-extension-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Sören Parton
 * @package   ElasticSearchExtensionModule
 * @since     1.0.0
 */
return [
    'ElasticSearchExtension plugin loaded' => 'ElasticSearchExtension plugin loaded',
];
