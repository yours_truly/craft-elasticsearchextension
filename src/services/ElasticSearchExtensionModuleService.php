<?php
/**
 * ElasticSearchExtension module for Craft CMS 3.x
 *
 * Extend the craft ES Plugin
 *
 * @link      soerenparton.de
 * @copyright Copyright (c) 2021 Sören Parton
 */

namespace yourstruly\elasticsearchextensionmodule\services;

use craft\elements\Entry;

use craft\events\RegisterGqlTypesEvent;
use craft\gql\base\GeneratorInterface;
use craft\gql\GqlEntityRegistry;
use craft\gql\TypeLoader;
use craft\gql\types\Query;
use craft\helpers\ArrayHelper;
use craft\helpers\StringHelper;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\Error;
use GraphQL\GraphQL;
use GraphQL\Type\Definition\ObjectType as GqlObjectType;
use lhs\elasticsearch\Elasticsearch;
use lhs\elasticsearch\queries\ElasticsearchQuery;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Query\Entry as EntryQuery;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces\Entry as EntryInterface;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types\Entry as EntryType;
use craft\errors\InvalidFieldException;
use craft\events\ExecuteGqlQueryEvent;
use craft\events\RegisterGqlQueriesEvent;
use craft\helpers\Gql;
use GraphQL\Type\Schema;
use lhs\elasticsearch\events\SearchEvent;
use lhs\elasticsearch\records\ElasticsearchRecord;
use yourstruly\elasticsearchextensionmodule\ElasticSearchExtensionModule;

use Craft;
use craft\base\Component;
use yourstruly\elasticsearchextensionmodule\services\ApplicabilityChecker\ApplicabilityCheckerInterface;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\Factory\BeforeSaveTransformFactory;
use yourstruly\elasticsearchextensionmodule\services\BeforeSearch\QueryParser;
use yourstruly\elasticsearchextensionmodule\services\Factory\CraftToElasticSearchAdapterFactory;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types\Image;
use yii\base\Event;

/**
 * ElasticSearchExtensionModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Sören Parton
 * @package   ElasticSearchExtensionModule
 * @since     1.0.0
 */
class ElasticSearchExtensionModuleService extends Component
{
    private $config;
    /** @var CraftToElasticSearchAdapterFactory */
    private $factory;
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->factory = new CraftToElasticSearchAdapterFactory();
        unset($config['additionalFields']);
        unset($config['esEnabled']);
        parent::__construct($config);
    }
    // Public Methods
    // =========================================================================
    public function isElasticSearchEnabled() {
        return (bool) $this->config['esEnabled'];
    }

    public function registerSearchFields(ElasticsearchQuery $query) {
        $searchFields = $query->getSearchFields();
        foreach ($this->config['additionalFields'] as $fieldHandle => $additionalField) {

            if (isset($additionalField['registerAsSearchField'])) {
                if ($additionalField['registerAsSearchField'] === true) {
                    $this->removeExistingSearchField($searchFields, $fieldHandle);
                    $searchFields[] = $fieldHandle;
                }
                elseif (is_array($additionalField['registerAsSearchField']) && isset($additionalField['registerAsSearchField']['boost'])) {
                    $this->removeExistingSearchField($searchFields, $fieldHandle);
                    $searchFields[] = $fieldHandle . '^' . $additionalField['registerAsSearchField']['boost'];
                }
            }
        }
        $query->setSearchFields($searchFields);
    }

    private function removeExistingSearchField(&$searchFields, $fieldHandle) {
        $searchFields = array_values(array_filter($searchFields, function($searchField) use ($fieldHandle) {
            return strpos($searchField, $fieldHandle) === false;
        }));
    }

    /**
     * @return ElasticsearchQuery
     * @throws \lhs\elasticsearch\exceptions\IndexElementException
     */
    public function buildQuery() {
        /** @var ElasticsearchQuery $query */
        $query = Elasticsearch::getInstance()->service->buildQuery();
        //Register Filters makes the query object filterable e.g. by query.section('article). ...
        $this->registerFilters($query);
        $this->registerSearchFields($query);
        return $query;
    }

    public function registerFilters(ElasticsearchQuery $query) {
        foreach ($this->config['additionalFields'] as $fieldHandle => $additionalField) {

            if (isset($additionalField['registerFilterInQuery'])) {
                $filterConfig = $additionalField['registerFilterInQuery'];
                $searchHandle = $filterConfig['searchHandle'] ??  $fieldHandle;
                if (isset($filterConfig['buildCallback'])) {
                    $query->registerFilter([
                        'searchHandle' => $searchHandle,
                        'buildCallback' => $filterConfig['buildCallback']
                    ]);
                }
                else {
                    $fieldHandle = $filterConfig['fieldHandle'] ??  $fieldHandle;
                    $esType = $filterConfig['esFilterType'];
                    $query->registerFilter([
                        'searchHandle' => $searchHandle,
                        'esFilterType' => $esType,
                        'fieldHandle' => $fieldHandle
                    ]);
                }

            }
        }
    }
    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     ElasticSearchExtensionModule::$instance->elasticSearchExtensionModuleService->exampleService()
     *
     * @return mixed
     */
    public function onInit(Event $event) {
        /** @var ElasticsearchRecord $esRecord */
        $esRecord = $event->sender;
        foreach ($this->config['additionalFields'] as $fieldHandle => $fieldConfig) {
            if (!in_array($fieldHandle, $esRecord->attributes())) {
                $esRecord->addAttributes([$fieldHandle]);
            }

        }
    }


    public function onBeforeSearch(SearchEvent $event) {
        /** @var ElasticsearchRecord $esRecord */
        $esRecord = $event->sender;
        $query = $event->query;
        // Customise the query params
        $queryParser = new QueryParser();
        $queryParser->parseQuery($query);


        $queryParams = $esRecord->getQueryParams($queryParser->getQueryPart());
        //$queryParams = ['match' => ['_all' => $query]];
        //$queryParams['bool']['must'][0]['multi_match']['fields'] = ArrayHelper::merge($queryParams['bool']['must'][0]['multi_match']['fields'], ['color']);
        $filters = $queryParser->getFilters();
        foreach ($filters as $filter) {
            $queryParams['bool']['must'][] = [
                $filter['operator'] => [
                    $filter['field'] => $filter['value']
                ]
            ];
        }
        $esRecord->setQueryParams($queryParams);
        // Customise the highlighter params
        $highlightParams = $esRecord->getHighlightParams();
        //$highlightParams['fields'] = ArrayHelper::merge($highlightParams['fields'], ['color' => (object)[]]);
        $esRecord->setHighlightParams($highlightParams);
    }




    public function onBeforeCreateIndex (Event $event) {
        /** @var ElasticsearchRecord $esRecord */
        $esRecord = $event->sender;
        $schema = $esRecord->getSchema();
        // Modify the original schema to add the additional field
        foreach ($this->config['additionalFields'] as $fieldHandle => $fieldConfig) {
            if (isset($fieldConfig['indexDefinition'])) {
                $schema['mappings']['properties'][$fieldHandle] = $fieldConfig['indexDefinition'];
            }

        }

        $esRecord->setSchema($schema);
    }

    public function onBeforeSave (Event $event) {
        /** @var ElasticsearchRecord $esRecord */
        $esRecord = $event->sender;
        $element = $esRecord->getElement();

        foreach ($this->config['additionalFields'] as $fieldHandle => $fieldConfig) {
            $recordFieldHandle = $fieldHandle;
            if (isset($fieldConfig['overrideSourceFieldHandle'])) {
                $fieldHandle = $fieldConfig['overrideSourceFieldHandle'];
            }

            $applicabilityChecker = $this->factory->createApplicabilityChecker($fieldHandle, $fieldConfig);
            if ($applicabilityChecker && $applicabilityChecker->isApplicable($element)) {
                $valueDeterminator = $this->factory->createValueDeterminator($fieldHandle, $fieldConfig);
                if ($valueDeterminator) {
                    $value = $valueDeterminator->determineValue($element);
                    $esRecord->{$recordFieldHandle} = $value;
                }

            }
        }
    }

    public function resultFormatter(array $formattedResult, $result) {
        foreach ($this->config['additionalFields'] as $fieldHandle => $fieldConfig) {
            $formatter = $this->factory->createFormatter($fieldHandle, $fieldConfig);
            $formattedResult[$fieldHandle] = $formatter ? $formatter->format($result->{$fieldHandle}) : $result->{$fieldHandle};
        }
        return $formattedResult;
    }


}
