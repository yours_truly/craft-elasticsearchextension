<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator;

use craft\base\Element;
use craft\elements\Entry;
use craft\errors\InvalidFieldException;

class EntryTypeValueDeterminator  implements ValueDeterminatorInterface
{
    public function __construct()
    {
    }
    public function determineValue(Element $element)
    {
        if ($element instanceof Entry) {
            /** @var Entry $element */
            try {
                return $element->type->handle;
            }
            catch (InvalidFieldException $e) {
                return '';
            }
        }
        return '';
    }

}