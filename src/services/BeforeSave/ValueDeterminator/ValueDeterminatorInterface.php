<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator;

use craft\base\Element;

interface ValueDeterminatorInterface
{
    public function determineValue(Element $element);
}