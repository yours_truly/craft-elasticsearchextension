<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types;

use craft\elements\Entry as EntryElement;
use craft\gql\base\ObjectType;
use craft\gql\GqlEntityRegistry;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces\Entry as EntryInterface;
use craft\gql\types\DateTime;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ObjectType as GqlObjectType;
use GraphQL\Type\Definition\Type;

/**
 * Class Entry
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.3.0
 */
class Entry extends GqlObjectType
{
    private static $additionalFieldConfig = [];
    /** @var callable[]  */
    private static $additionalResolverConfig = [];

    public function __construct(array $config = [])
    {
        $config['fields'] = [
            'id' => [
                'type' => Type::id(),
                ''
            ],
        ];
        $config['interfaces'] = [
            EntryInterface::getType(),
        ];
        foreach (self::$additionalFieldConfig as $key => $fieldConfig) {
            $config['fields'][$key] = $fieldConfig;
        }
        $config['resolveField'] = function ($rootValue, $args, $context, ResolveInfo $info) {
            return $this->resolve($rootValue, $args, $context, $info);
        };

        parent::__construct($config);
    }

    public static function getFieldConfig() {
        return self::$additionalFieldConfig;
    }
    public static function addFieldConfig($key, $config, callable $resolver) {
        self::$additionalFieldConfig[$key] = $config;
        self::$additionalResolverConfig[$key] = $resolver;
    }
    /**
     * @inheritdoc
     */
    protected function resolve($source, $arguments, $context, ResolveInfo $resolveInfo)
    {
        /** @var EntryElement $source */
        $fieldName = $resolveInfo->fieldName;

        switch ($fieldName) {
            case 'id':
                return $source->id();
            default:
                if (isset(self::$additionalResolverConfig[$fieldName])) {
                    return (self::$additionalResolverConfig[$fieldName])($source);
                }
                else {
                    /** @todo maybe throw exception here */
                    return null;
                }
        }
    }


}
