<?php
/**
 * AuthZeroUserImport module for Craft CMS 3.x
 *
 * Imports Auth0 userdata
 *
 * @link      soerenparton.de
 * @copyright Copyright (c) 2021 Sören Parton
 */

namespace yourstruly\elasticsearchextensionmodule\controllers;

use craft\elements\Entry;
use craft\elements\User;
use craft\web\Controller;
use craft\web\View;
use enupal\socializer\elements\Provider;
use enupal\socializer\Socializer;
use modules\authzerouserimportmodule\AuthZeroUserImportModule;

use Craft;



/**
 * @author    Sören Parton
 * @package   AuthZeroUserImportModule
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected array|bool|int $allowAnonymous = ['demo'];

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function beforeAction($action) : bool
    {
        if ($action->id == 'register') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionDemo() {
        return $this->renderTemplate('elasticsearchextensionmodule/demo', []);
    }

}
