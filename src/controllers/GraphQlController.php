<?php
/**
 * AuthZeroUserImport module for Craft CMS 3.x
 *
 * Imports Auth0 userdata
 *
 * @link      soerenparton.de
 * @copyright Copyright (c) 2021 Sören Parton
 */

namespace yourstruly\elasticsearchextensionmodule\controllers;

use craft\elements\Entry;
use craft\elements\User;
use craft\helpers\DateTimeHelper;
use craft\helpers\Gql as GqlHelper;
use craft\web\Controller;
use craft\web\View;
use enupal\socializer\elements\Provider;
use enupal\socializer\Socializer;
use GraphQL\Type\Schema;
use modules\authzerouserimportmodule\AuthZeroUserImportModule;

use Craft;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\QueryType;


/**
 * @author    Sören Parton
 * @package   AuthZeroUserImportModule
 * @since     1.0.0
 */
class GraphQlController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected array|bool|int $allowAnonymous = ['search'];

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function beforeAction($action) : bool
    {
        if ($action->id == 'search') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionSearch() {
        //First: Forward request to Craft Request if ES is not available
        if (!$this->isElasticSearchAvailable()) {
            return $this->run('/graphql/api', []);
        }

        $gqlService = Craft::$app->getGql();
        $token = $gqlService->getPublicToken();
        $token->lastUsed = DateTimeHelper::currentUTCDateTime();
        $gqlService->saveToken($token);
        $schemaDef = $gqlService->getSchemaDef($token->getSchema(), true);


        return $this->asJson($schemaDef->getTypeMap());

        $queryString = Craft::$app->request->getParam('query');
        $variables = Craft::$app->request->getParam('variables');

        $schema = new Schema([
            'query' => new QueryType(),
            'typeLoader' => static fn (string $name): Type => Types::byTypeName($name),
        ]);

        return $this->asJson(['success' => true]);
    }

    private function isElasticSearchAvailable() {
        return true;
    }

}
