<?php

namespace yourstruly\elasticsearchextensionmodule\services\Factory;

use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\ElementLoaderFormatter;
use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\FormatterInterface;
use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\MockDependency\SubItemFormatter\AssetFormatter;
use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\MockDependencyFormatter;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker\ApplicabilityCheckerInterface;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker\EntryHasFieldChecker;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker\IsClassChecker;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\MatrixBlockValueDeterminator;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter\MultifieldObjectStrategy;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter\SingleFieldStrategy;
use yourstruly\elasticsearchextensionmodule\services\Factory\CraftToElasticSearchAdapterFactoryInterface;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\EntryTypeValueDeterminator;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy\ImplodeStrategy;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy\KeepAsArrayStrategy;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\RelationValueDeterminator;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\SimpleAttributeValueDeterminator;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ValueDeterminatorInterface;

class CraftToElasticSearchAdapterFactory implements CraftToElasticSearchAdapterFactoryInterface
{
    public function createApplicabilityChecker(string $fieldHandle, array $fieldConfig): ?ApplicabilityCheckerInterface
    {
        if (!isset($fieldConfig['applicabilityStrategy'])) {
            return null;
        }
        $applicabilityConfig = $fieldConfig['applicabilityStrategy'];
        switch ($applicabilityConfig['strategy']) {
            case 'classChecker':
                $applicabilityChecker = new IsClassChecker($applicabilityConfig['config']['class']);
                break;
            case 'entryHasField':
                $applicabilityChecker = new EntryHasFieldChecker($applicabilityConfig['config']['field'] ?? $fieldHandle);
                break;

        }
        return $applicabilityChecker;
    }
    public function createValueDeterminator(string $fieldHandle, array $fieldConfig): ?ValueDeterminatorInterface
    {
        $valueConfig = $fieldConfig['valueStrategy'];
        switch ($valueConfig['strategy']) {

            case 'relationValue':
                $reduceStrategy = $this->createReduceStrategy($valueConfig['config']['arrayReductionStrategy']);
                $valueDeterminator = new RelationValueDeterminator($fieldHandle,$valueConfig['config']['queryValues'], $reduceStrategy);
                break;
            case 'matrixBlockField':
                $reduceStrategy = $this->createReduceStrategy($valueConfig['config']['arrayReductionStrategy']);
                $valueDeterminator = new MatrixBlockValueDeterminator($valueConfig['config']['field'] ?? $fieldHandle,
                    $valueConfig['config']['blockFieldHandle'], $valueConfig['config']['blockTypeHandle'], $reduceStrategy);
                break;
            case 'simpleAttribute':
                $handle = $valueConfig['config']['pathToField'] ?? $fieldHandle;
                $valueDeterminator = new SimpleAttributeValueDeterminator($handle);
                break;
            case 'entryType':
                $valueDeterminator = new EntryTypeValueDeterminator();
                break;
        }
        return $valueDeterminator;
    }

    public function createFormatter(string $fieldHandle, array $fieldConfig): ?FormatterInterface
    {
        if (!isset($fieldConfig['formatter'])) {
            return null;
        }
        $formatterConfig = $fieldConfig['formatter'];
        $formatter = null;
        switch ($formatterConfig['strategy']) {
            case 'elementLoader':
                $loaderClass = $formatterConfig['config']['class'];
                $formatter = new ElementLoaderFormatter($loaderClass);
                break;
            case 'mockDependency':
                $subItemFormatter = isset($formatterConfig['config']['subItemFormatter']) ?
                    $this->createSubItemFormatter($formatterConfig['config']['subItemFormatter']) : null ;
                $formatter = new MockDependencyFormatter($subItemFormatter);
                break;
        }
        return $formatter;

    }

    private function createSubItemFormatter($class) {
        switch ($class) {
            case 'asset':
                return new AssetFormatter();
            default:
                throw new \Exception('Invalid configuration option for subItemFormatter: ' . $class);
        }
    }

    private function createReduceStrategy(array $config) {
        $objectToIndexStrategy = $this->createObjectToIndexStrategy($config['config']['objectConverterStrategy']);
        switch ($config['strategy']) {
            case 'implode':
                $reduceStrategy = new ImplodeStrategy($config['config']['separator'], $objectToIndexStrategy);
                break;
            case 'keepAsArray':
                $reduceStrategy = new KeepAsArrayStrategy($objectToIndexStrategy);
                break;
        }
        return $reduceStrategy;
    }
    private function createObjectToIndexStrategy(array $config) {
        switch ($config['strategy']) {
            case 'single':
                $strategy = new SingleFieldStrategy($config['config']['field']);
                break;
            case 'multi':
                $strategy = new MultifieldObjectStrategy($config['config']['fields']);
                break;
        }
        return $strategy;
    }

}