<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker;

use craft\base\Element;
use craft\elements\Entry;
use craft\errors\InvalidFieldException;

class EntryHasFieldChecker implements ApplicabilityCheckerInterface
{
    /** @var string */
    private $fieldHandle;
    public function __construct($fieldHandle)
    {
        $this->fieldHandle = $fieldHandle;
    }
    public function isApplicable(Element $element): bool
    {
        if ($element instanceof Entry) {
            /** @var Entry $element */
            try {
                return in_array($this->fieldHandle, $element->type->fields()) || in_array($this->fieldHandle, array_keys($element->fields()));
            }
            catch (InvalidFieldException $e) {
                return false;
            }
        }
        return false;

    }

}