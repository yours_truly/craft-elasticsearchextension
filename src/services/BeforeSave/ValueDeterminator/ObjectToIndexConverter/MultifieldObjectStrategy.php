<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter;

class MultifieldObjectStrategy implements ObjectToIndexConverterInterface
{
    private $fieldConfig;
    public function __construct($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }

    public function convertToIndexableValue($object)
    {
        $result = [];
        foreach ($this->fieldConfig as $fieldHandle) {
            $result[$fieldHandle] =  $object->{$fieldHandle};
        }
        return $result;
    }


}