<?php

namespace yourstruly\elasticsearchextensionmodule\services;
use Craft;
use craft\errors\GqlException;
use craft\events\RegisterGqlDirectivesEvent;
use craft\events\RegisterGqlMutationsEvent;
use craft\events\RegisterGqlQueriesEvent;
use craft\events\RegisterGqlTypesEvent;
use craft\gql\base\Directive;
use craft\gql\base\GeneratorInterface;
use craft\gql\base\InterfaceType;
use craft\gql\directives\FormatDateTime;
use craft\gql\directives\Markdown;
use craft\gql\directives\ParseRefs;
use craft\gql\directives\Transform;
use craft\gql\GqlEntityRegistry;
use craft\gql\interfaces\Element as ElementInterface;
use craft\gql\interfaces\elements\Asset as AssetInterface;
use craft\gql\interfaces\elements\Category as CategoryInterface;
use craft\gql\interfaces\elements\Entry as EntryInterface;
use craft\gql\interfaces\elements\GlobalSet as GlobalSetInterface;
use craft\gql\interfaces\elements\MatrixBlock as MatrixBlockInterface;
use craft\gql\interfaces\elements\Tag as TagInterface;
use craft\gql\interfaces\elements\User as UserInterface;
use craft\gql\mutations\Asset as AssetMutation;
use craft\gql\mutations\Category as CategoryMutation;
use craft\gql\mutations\Entry as EntryMutation;
use craft\gql\mutations\GlobalSet as GlobalSetMutation;
use craft\gql\mutations\Ping as PingMutation;
use craft\gql\mutations\Tag as TagMutation;
use craft\gql\queries\Asset as AssetQuery;
use craft\gql\queries\Category as CategoryQuery;
use craft\gql\queries\Entry as EntryQuery;
use craft\gql\queries\GlobalSet as GlobalSetQuery;
use craft\gql\queries\Ping as PingQuery;
use craft\gql\queries\Tag as TagQuery;
use craft\gql\queries\User as UserQuery;
use craft\gql\TypeLoader;
use craft\gql\TypeManager;
use craft\gql\types\DateTime;
use craft\gql\types\Mutation;
use craft\gql\types\Number;
use craft\gql\types\Query;
use craft\gql\types\QueryArgument;
use craft\services\Gql;
use GraphQL\GraphQL;
use GraphQL\Type\Schema;

class GqlCraftSimulatorService
{
    /**
     * Flushing the singleton registries. Useful before passing the responsibility of executing the gql query back to
     * Craft.
     */
    public function flushRegistries() {
        TypeLoader::flush();
        GqlEntityRegistry::flush();
        TypeManager::flush();
    }

    /**
     * Simulate the Craft schema without using the actual craft GQL service.
     * If we had used it, we might have run into caching/registry issues when falling back to the Craft GQL and
     * thus rebuilding the schema
     * @return Schema
     * @throws GqlException
     */
    public function buildCraftSchema() {
        // Either cached version was not found or we need a pre-built schema.
        $registeredTypes = $this->_registerGqlTypes();
        $this->_registerGqlQueries();
        $this->_registerGqlMutations();

        $schemaConfig = [
            'typeLoader' => TypeLoader::class . '::loadType',
            'query' => TypeLoader::loadType('Query'),
            'mutation' => TypeLoader::loadType('Mutation'),
            'directives' => $this->_loadGqlDirectives(),
        ];


        foreach ($registeredTypes as $registeredType) {
            if (method_exists($registeredType, 'getTypeGenerator')) {
                /** @var GeneratorInterface $typeGeneratorClass */
                $typeGeneratorClass = $registeredType::getTypeGenerator();

                if (is_subclass_of($typeGeneratorClass, GeneratorInterface::class)) {
                    foreach ($typeGeneratorClass::generateTypes() as $type) {
                        $schemaConfig['types'][] = $type;
                    }
                }
            }
        }

        try {
            $schemaDef = new Schema($schemaConfig);
            $schemaDef->getTypeMap();
        } catch (\Throwable $exception) {
            throw new GqlException('Failed to validate the GQL Schema - ' . $exception->getMessage());
        }


        return $schemaDef;
    }


    /**
     * Copied from Craft GQL Service
     * Register GraphQL types
     *
     * @return array the list of registered types.
     */
    private function _registerGqlTypes(): array
    {
        $typeList = [
            // Scalars
            /*DateTime::class,
            Number::class,
            QueryArgument::class,

            // Interfaces
            ElementInterface::class,
            EntryInterface::class,
            MatrixBlockInterface::class,
            AssetInterface::class,
            UserInterface::class,
            GlobalSetInterface::class,
            CategoryInterface::class,
            TagInterface::class,*/
        ];

        $event = new RegisterGqlTypesEvent([
            'types' => $typeList,
        ]);

        //Even though we want to simulate the craft query as exactly as possible first before modifying it,
        //this trigger will cause types to be registered more than once (non singletons) throwing an error
        //in a fallback situation. Luckily we don't need it.
        //Craft::$app->getGql()->trigger(Gql::EVENT_REGISTER_GQL_TYPES, $event);

        foreach ($event->types as $type) {
            /** @var InterfaceType $type */
            TypeLoader::registerType($type::getName(), $type . '::getType');
        }

        return $event->types;
    }

    /**
     * Copied from Craft GQL Service
     * Get GraphQL query definitions
     */
    private function _registerGqlQueries()
    {
        $queryList = [
            // Queries
            PingQuery::getQueries(),
            EntryQuery::getQueries(),
            AssetQuery::getQueries(),
            UserQuery::getQueries(),
            GlobalSetQuery::getQueries(),
            CategoryQuery::getQueries(),
            TagQuery::getQueries(),
        ];


        $event = new RegisterGqlQueriesEvent([
            'queries' => array_merge(...$queryList),
        ]);

        Craft::$app->getGql()->trigger(Gql::EVENT_REGISTER_GQL_QUERIES, $event);

        TypeLoader::registerType('Query', function() use ($event) {
            return call_user_func(Query::class . '::getType', $event->queries);
        });
    }

    /**
     * Copied from Craft GQL Service
     * Get GraphQL mutation definitions
     */
    private function _registerGqlMutations()
    {
        $mutationList = [
            // Mutations
            PingMutation::getMutations(),
            EntryMutation::getMutations(),
            TagMutation::getMutations(),
            CategoryMutation::getMutations(),
            GlobalSetMutation::getMutations(),
            AssetMutation::getMutations(),
        ];


        $event = new RegisterGqlMutationsEvent([
            'mutations' => array_merge(...$mutationList),
        ]);

        Craft::$app->getGql()->trigger(Gql::EVENT_REGISTER_GQL_MUTATIONS, $event);

        TypeLoader::registerType('Mutation', function() use ($event) {
            return call_user_func(Mutation::class . '::getType', $event->mutations);
        });
    }

    /**
     * Copied from Craft GQL Service
     * Get GraphQL query definitions
     *
     * @return Directive[]
     */
    private function _loadGqlDirectives(): array
    {
        $directiveClasses = [
            // Directives
            FormatDateTime::class,
            Markdown::class,
            ParseRefs::class,
        ];

        if (!\Craft::$app->getConfig()->getGeneral()->disableGraphqlTransformDirective) {
            $directiveClasses[] = Transform::class;
        }

        $event = new RegisterGqlDirectivesEvent([
            'directives' => $directiveClasses,
        ]);

        Craft::$app->getGql()->trigger(Gql::EVENT_REGISTER_GQL_DIRECTIVES, $event);

        $directives = GraphQL::getStandardDirectives();

        foreach ($event->directives as $directive) {
            /** @var Directive $directive */
            $directives[] = $directive::create();
        }

        return $directives;
    }

}