<?php

namespace yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter;

use craft\base\Element;
use craft\elements\Asset;
use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\Type\MockCraftDependency;

class MockDependencyFormatter implements FormatterInterface
{
    /** @var FormatterInterface */
    private $subItemFormatter;
    public function __construct($subItemFormatter)
    {
        $this->subItemFormatter = $subItemFormatter;
    }

    public function format($result)
    {

        if ($this->subItemFormatter) {
            $formattedResults = [];
            if (is_array($result)) {
                foreach ($result as $item) {
                    $formattedResults[] = $this->subItemFormatter->format($item);
                }
            }

        }
        else {
            $formattedResults = $result;
        }

        return new MockCraftDependency($formattedResults);
    }
}