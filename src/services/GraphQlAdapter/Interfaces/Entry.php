<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Interfaces;

use craft\elements\Entry as EntryElement;
use craft\gql\base\ObjectType;
use craft\gql\GqlEntityRegistry;
use craft\gql\interfaces\elements\Entry as EntryInterface;
use craft\gql\types\DateTime;
use craft\gql\types\generators\EntryType;
use lhs\elasticsearch\records\ElasticsearchRecord;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Generators\EntryTypeGenerator;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ObjectType as GqlObjectType;
use GraphQL\Type\Definition\Type;

/**
 * Class Entry
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.3.0
 */
class Entry extends InterfaceType
{
    private static $additionalFieldConfig = [];
    /** @var callable[]  */
    private static $additionalResolverConfig = [];

    public function __construct(array $config = [])
    {
        $config['fields'] = [
            'id' => [
                'type' => Type::id(),
                ''
            ],
        ];
        foreach (\yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types\Entry::getFieldConfig() as $key => $fieldConfig) {
            $config['fields'][$key] = $fieldConfig;
        }
//        $config['resolveField'] = function ($rootValue, $args, $context, ResolveInfo $info) {
//            /** @var EntryElement $source */
//            $fieldName = $info->fieldName;
//
//            switch ($fieldName) {
//                case 'id':
//                    return $source->id();
//                default:
//                    if (isset(self::$additionalResolverConfig[$fieldName])) {
//                        return (self::$additionalResolverConfig[$fieldName])($source);
//                    }
//                    else {
//                        /** @todo maybe throw exception here */
//                        return null;
//                    }
//            }
//        };
        $config['name'] = self::getName();
        $config['resolveType'] = self::class . '::resolveTypeName';
        parent::__construct($config);
    }
    /**
     * @inheritdoc
     */
    public static function getTypeGenerator(): string
    {
        return EntryTypeGenerator::class;
    }
    public static function getType(): self
    {
        return GqlEntityRegistry::getEntity(self::getName()) ?: GqlEntityRegistry::createEntity(self::getName(), new self());
    }


    public static function getName() {
        return 'ElasticSearchEntryInterface';
    }

    /**
     * Filters out all types our generator is substituting
     * @param array $types
     */
    public static function removeConflictingTypes(array &$types) {
        $types = array_filter($types, function($type) {
           return !($type instanceof \craft\gql\types\elements\Entry);
        });
    }

    /**
     * Resolve an element type name.
     *
     * @param ElasticsearchRecord $element
     * @return string
     * @since 3.5.0
     */
    public static function resolveTypeName(ElasticsearchRecord $esRecord): string
    {
        return GqlEntityRegistry::prefixTypeName(sprintf('%s_%s_Entry', $esRecord->sectionHandle, $esRecord->type));
    }


}
