<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator;

use craft\base\Element;


class SimpleAttributeValueDeterminator  implements ValueDeterminatorInterface
{
    private $fieldHandle;
    public function __construct(string $fieldHandle)
    {
        $this->fieldHandle = $fieldHandle;
    }
    public function determineValue(Element $element)
    {
        $fieldPathParts = explode('.', $this->fieldHandle);
        $value = $element;
        foreach ($fieldPathParts as $field) {
            $value = $value->{$field};
        }
        return $value;
    }
}