<?php

namespace yourstruly\elasticsearchextensionmodule\services\Factory;

use yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter\FormatterInterface;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker\ApplicabilityCheckerInterface;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ValueDeterminatorInterface;

interface CraftToElasticSearchAdapterFactoryInterface
{
    public function createApplicabilityChecker(string $fieldHandle, array $fieldConfig): ?ApplicabilityCheckerInterface;
    public function createValueDeterminator(string $fieldHandle, array $fieldConfig): ?ValueDeterminatorInterface;
    public function createFormatter(string $fieldHandle, array $fieldConfig): ?FormatterInterface;
}