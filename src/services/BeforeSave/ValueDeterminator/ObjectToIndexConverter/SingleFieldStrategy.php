<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter;

class SingleFieldStrategy implements ObjectToIndexConverterInterface
{
    private $fieldHandle;
    public function __construct($fieldHandle)
    {
        $this->fieldHandle = $fieldHandle;
    }

    public function convertToIndexableValue($object)
    {
        return $object->{$this->fieldHandle};
    }


}