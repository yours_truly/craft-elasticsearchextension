/**
 * ElasticSearchExtension module for Craft CMS
 *
 * ElasticSearchExtension JS
 *
 * @author    Sören Parton
 * @copyright Copyright (c) 2021 Sören Parton
 * @link      soerenparton.de
 * @package   ElasticSearchExtensionModule
 * @since     1.0.0
 */
