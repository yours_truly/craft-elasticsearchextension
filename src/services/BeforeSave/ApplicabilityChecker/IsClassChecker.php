<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker;

use craft\base\Element;
use craft\elements\Entry;
use craft\errors\InvalidFieldException;

class IsClassChecker implements ApplicabilityCheckerInterface
{
    /** @var string  */
    private $class;
    public function __construct(string $class)
    {
        $this->class = $class;
    }
    public function isApplicable(Element $element): bool
    {
       return is_a($element, $this->class);
    }

}