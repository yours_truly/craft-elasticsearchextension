<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Resolvers;

use Craft;
use craft\base\EagerLoadingFieldInterface;
use craft\base\ElementInterface;
use craft\base\GqlInlineFragmentFieldInterface;
use craft\db\Table;
use craft\elements\db\ElementQuery;
use craft\elements\Entry as EntryElement;
use craft\gql\ArgumentManager;
use craft\gql\base\ElementResolver;
use craft\gql\ElementQueryConditionBuilder;
use craft\helpers\Db;
use craft\helpers\Gql as GqlHelper;
use GraphQL\Type\Definition\ResolveInfo;
use lhs\elasticsearch\Elasticsearch;
use lhs\elasticsearch\queries\ElasticsearchQuery;
use lhs\elasticsearch\records\ElasticsearchRecord;
use yourstruly\elasticsearchextensionmodule\ElasticSearchExtensionModule;
use Illuminate\Support\Collection;

/**
 * Class Entry
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.3.0
 */
class Entry extends ElementResolver
{
    static $argumentMap = [
      'search' => 'searchString'
    ];
    public static function addMappedArgument($key, $value) {
        self::$argumentMap[$key] = $value;
    }
    /**
     * @inheritdoc
     */
    public static function prepareQuery(mixed $source, array $arguments, ?string $fieldName = null) : mixed
    {
        // If this is the beginning of a resolver chain, start fresh
        if ($source === null) {
            /** @var ElasticsearchQuery $query */
            $query = Elasticsearch::getInstance()->service->buildQuery();
            ElasticSearchExtensionModule::getInstance()->elasticSearchExtensionModuleService->registerFilters($query);
            ElasticSearchExtensionModule::getInstance()->elasticSearchExtensionModuleService->registerSearchFields($query);
            // If not, get the prepared element query
        } else {
            $query = $source->$fieldName;
        }

        // If it's preloaded, it's preloaded.
        if (is_array($query)) {
            return $query;
        }

        foreach ($arguments as $key => $value) {
            if (isset(self::$argumentMap[$key])) {
                $key = self::$argumentMap[$key];
            }
            $query->$key($value);
        }

        $pairs = GqlHelper::extractAllowedEntitiesFromSchema('read');

        //$query->andWhere(['in', 'entries.sectionId', array_values(Db::idsByUids(Table::SECTIONS, $pairs['sections']))]);
        //$query->andWhere(['in', 'entries.typeId', array_values(Db::idsByUids(Table::ENTRYTYPES, $pairs['entrytypes']))]);

        return $query;
    }
    /**
     * @inheritdoc
     */
    public static function resolve(mixed $source, array $arguments, mixed $context, ResolveInfo $resolveInfo) : mixed
    {
        $query = self::prepareElementQuery($source, $arguments, $context, $resolveInfo);
        $value =  $query->all();
        return GqlHelper::applyDirectives($source, $resolveInfo, $value);
    }

    protected static function prepareElementQuery(mixed $source, array $arguments, ?array $context, ResolveInfo $resolveInfo): ElementQuery|Collection
    {
        /** @var ArgumentManager $argumentManager */
        $argumentManager = empty($context['argumentManager']) ? Craft::createObject(['class' => ArgumentManager::class]) : $context['argumentManager'];
        $arguments = $argumentManager->prepareArguments($arguments);

        $fieldName = GqlHelper::getFieldNameWithAlias($resolveInfo, $source, $context);

        $query = static::prepareQuery($source, $arguments, $fieldName);

        // If that's already preloaded, then, uhh, skip the preloading?
        if (is_array($query)) {
            return $query;
        }

        $parentField = null;

        if ($source instanceof ElementInterface) {
            $fieldContext = $source->getFieldContext();
            $field = Craft::$app->getFields()->getFieldByHandle($fieldName, $fieldContext);

            // This will happen if something is either dynamically added or is inside an block element that didn't support eager-loading
            // and broke the eager-loading chain. In this case Craft has to provide the relevant context so the condition builder knows where it's at.
            if (($fieldContext !== 'global' && $field instanceof GqlInlineFragmentFieldInterface) || $field instanceof EagerLoadingFieldInterface) {
                $parentField = $field;
            }
        }

        //Removed Condition builder present in ElementResolver base implementation

        // Apply max result config
        $maxGraphqlResults = Craft::$app->getConfig()->getGeneral()->maxGraphqlResults;

        // Reset negative limit to zero
        if ((int)$query->limit < 0) {
            $query->limit(0);
        }

        if ($maxGraphqlResults > 0) {
            $queryLimit = is_null($query->limit) ? $maxGraphqlResults : min($maxGraphqlResults, $query->limit);
            $query->limit($queryLimit);
        }

        return $query;
    }

}
