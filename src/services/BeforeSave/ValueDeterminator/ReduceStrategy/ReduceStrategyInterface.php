<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy;

interface ReduceStrategyInterface
{
    public function reduceValue(array $elements);
}