<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ApplicabilityChecker;

use craft\base\Element;

interface ApplicabilityCheckerInterface
{
    public function isApplicable(Element $element): bool;
}