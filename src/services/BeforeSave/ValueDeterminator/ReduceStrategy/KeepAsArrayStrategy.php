<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy;

use craft\elements\Asset;
use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter\ObjectToIndexConverterInterface;

class KeepAsArrayStrategy extends AbstractReduceStrategy
{

    public function reduceValue(array $elements)
    {
        return array_map(function($element)  {return $this->objectConverter->convertToIndexableValue($element);}, $elements);
    }

}