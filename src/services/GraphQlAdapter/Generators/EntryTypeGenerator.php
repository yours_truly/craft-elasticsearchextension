<?php

namespace yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Generators;

use craft\elements\Entry as EntryElement;
use craft\gql\base\Generator;
use craft\gql\base\GeneratorInterface;
use craft\gql\base\ObjectType;
use craft\gql\base\SingleGeneratorInterface;
use craft\gql\GqlEntityRegistry;
use craft\gql\interfaces\elements\Entry as EntryInterface;
use craft\gql\TypeManager;
use GraphQL\Type\Definition\ObjectType as GqlObjectType;
use yourstruly\elasticsearchextensionmodule\services\GraphQlAdapter\Types\Entry as EntryType;
use craft\helpers\Gql as GqlHelper;
use craft\models\EntryType as EntryTypeModel;

/**
 * Class EntryType
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.3.0
 */
class EntryTypeGenerator implements GeneratorInterface
{
    /**
     * @inheritdoc
     */
    public static function generateTypes($context = null): array
    {
        $entryTypes = \Craft::$app->getSections()->getAllEntryTypes();
        $gqlTypes = [];

        foreach ($entryTypes as $entryType) {
            // Generate a type for each entry type
            $type = static::generateType($entryType);
            $gqlTypes[$type->name] = $type;
        }

        return $gqlTypes;
    }

    /**
     * @inheritdoc
     */
    public static function generateType($context): GqlObjectType
    {
        /** @var EntryTypeModel $entryType */
        $typeName = EntryElement::gqlTypeNameByContext($context);

//        if ($createdType = GqlEntityRegistry::getEntity($typeName)) {
//            return $createdType;
//        }

        return GqlEntityRegistry::createEntity($typeName, new EntryType([
            'name' => $typeName
        ]));
    }
}
