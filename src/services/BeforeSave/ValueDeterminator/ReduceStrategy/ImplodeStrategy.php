<?php

namespace yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ReduceStrategy;

use yourstruly\elasticsearchextensionmodule\services\BeforeSave\ValueDeterminator\ObjectToIndexConverter\ObjectToIndexConverterInterface;

class ImplodeStrategy extends AbstractReduceStrategy
{
    /** @var string  */
    private $separator;

    /**
     * @param string $separator
     * @param ObjectToIndexConverterInterface $objectToIndexConverter
     */
    public function __construct(string $separator, ObjectToIndexConverterInterface $objectToIndexConverter)
    {
        $this->separator = $separator;
        parent::__construct($objectToIndexConverter);
    }

    /**
     * @param array $elements
     * @return string
     */
    public function reduceValue(array $elements)
    {
        return implode($this->separator, array_map(function($element) {return $this->objectConverter->convertToIndexableValue($element);}, $elements));
    }
}