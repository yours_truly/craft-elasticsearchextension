<?php

namespace yourstruly\elasticsearchextensionmodule\services\AfterSearch\Formatter;

use craft\base\Element;
use craft\elements\Asset;

class ElementLoaderFormatter implements FormatterInterface
{
    private $loaderClass;
    public function __construct($loaderClass)
    {
        $this->loaderClass = $loaderClass;
    }

    public function format($result)
    {
        $r = new \ReflectionClass($this->loaderClass);
        /** @var Element $instance */
        $instance =  $r->newInstanceWithoutConstructor();
        return $instance::find()->id($result)->all();
    }
}